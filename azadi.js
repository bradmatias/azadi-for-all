// Who Stands Against the Law
// Team Azadi for All
// Nick Carlock
// Koosha Sevyani
// Brad Matias

canvas = document.getElementById('pTest');
context = canvas.getContext('2d');

var aura = false; //causes our aggro to expand when true, contract when false
//Key codes
var shiftCode = 16;
var upCode = 38;
var downCode = 40;
var leftCode = 37;
var rightCode = 39;
var enterCode = 13;
var ctrlCode = 17;
var rCode = 82;
var particles = []; //2d array of particle systems
var once = true;
var choice = 0; //1: wear, 2: do not wear
var world; //determines which world we are in
var flip = false; //tracks when to switch player position with ghost
var select = 1; //variable controlling player's preference - hijab or no
var hijab = true; //tracks our aggro state
var enemies = Array();
var objects = Array();
var walls = Array();
var gates = Array();
var plates = Array();
var player;
var level = 1;
var playerx = 0;
var playery = 0;
var prevX; //for player collision
var prevY;
var pillar = new Image();
pillar.src = "http://people.ucsc.edu/~ncarlock/azadi/pillar.png";
var winning = false; 
var endbg = new Image();
endbg.src = "http://people.ucsc.edu/~ncarlock/azadi/endScreen-01.png";


var tehranBackgrounds = {};
tehranBackgrounds[0] = new Image();
tehranBackgrounds[0].src = "http://people.ucsc.edu/~ncarlock/azadi/pilllarsT-01.png";
tehranBackgrounds[1] = new Image();
tehranBackgrounds[1].src = "http://i.imgur.com/XKRmWgb.png";
tehranBackgrounds[2] = new Image();
tehranBackgrounds[2].src = "http://i.imgur.com/ZUBkRPH.png";
tehranBackgrounds[3] = new Image();
tehranBackgrounds[3].src = "http://people.ucsc.edu/~ncarlock/azadi/postOffice-01.png";
tehranBackgrounds[4] = new Image();
tehranBackgrounds[4].src = "http://people.ucsc.edu/~ncarlock/azadi/room6T-01.png";
tehranBackgrounds[5] = new Image();
tehranBackgrounds[5].src = "http://people.ucsc.edu/~ncarlock/azadi/room4T-01.png";
tehranBackgrounds[6] = new Image();
tehranBackgrounds[6].src = "http://people.ucsc.edu/~ncarlock/azadi/room7T-01.png";


var parisBackgrounds = {};
parisBackgrounds[0] = new Image();
parisBackgrounds[0].src = "http://people.ucsc.edu/~ncarlock/azadi/pillarsP-01.png";
parisBackgrounds[1] = new Image();
parisBackgrounds[1].src = "http://i.imgur.com/uN7v3Dj.png";
parisBackgrounds[2] = new Image();
parisBackgrounds[2].src = "http://i.imgur.com/RTDQTnb.png";
parisBackgrounds[3] = new Image();
parisBackgrounds[3].src = "http://people.ucsc.edu/~ncarlock/azadi/postOfficeP-01.png";
parisBackgrounds[4] = new Image();
parisBackgrounds[4].src = "http://people.ucsc.edu/~ncarlock/azadi/room6P-01.png";
parisBackgrounds[5] = new Image();
parisBackgrounds[5].src = "http://people.ucsc.edu/~ncarlock/azadi/room4P-01.png";
parisBackgrounds[6] = new Image();
parisBackgrounds[6].src = "http://people.ucsc.edu/~ncarlock/azadi/room7P-01.png";


var keysPressed = {};
keysPressed[rightCode] = false;
keysPressed[leftCode] = false;
keysPressed[upCode] = false;
keysPressed[downCode] = false;
keysPressed[shiftCode] = false;
keysPressed[enterCode] = false;
keysPressed[ctrlCode] = false;
keysPressed[rCode] = false;



function keyDown(e) {
  if (e.keyCode == shiftCode){ //determines the way our aggro spreads/shrinks
    if (world == 1 && choice == 1)
      aura = true;
    else if (world ==1 && choice == 2)
      aura = false;
    else if (world ==2 && choice ==1)
      aura = false ;
    else
      aura = true ;
  }

  if (e.keyCode == ctrlCode && world == 1){
    world = 2;
    flip = true;
    if (choice ==1)
      aura = true ;
    else
      aura = false ;
  }
  
  else if (e.keyCode == rCode){
    reset();
  }

  else if (e.keyCode == ctrlCode && world == 2){
    world = 1;
    flip = true;
	if (choice == 1)
      aura = false;
    else
      aura = true;
  }

  if (e.keyCode in keysPressed){
  keysPressed[e.keyCode] = true;
}
}

function keyUp(e) {
  if (e.keyCode == shiftCode){
    if (world == 1 && choice == 1)
      aura = false;
    else if (world ==1 && choice == 2)
      aura = true;
    else if (world ==2 && choice ==1)
      aura = true ;
    else
      aura = false ;
  }
  if (e.keyCode in keysPressed) {  //clears our pressed keys
  keysPressed[e.keyCode] = false;
}
}

document.addEventListener('keydown', keyDown);
document.addEventListener('keyup', keyUp);

function Particle(xCoor, yCoor, radius, lifetime, direction, speed, color) {
  this.xCoor = xCoor;
  this.yCoor = yCoor;
  this.radius = radius;
  this.lifetime = lifetime;
  this.oglifetime = lifetime;//for saving what the original lifetime was
  this.reallife = lifetime*Math.random();//lifetime is used to hold the original lifetime and real will be altered
  this.direction = direction;
  this.speed = speed;
  this.color = color;
  this.hspeed = speed * Math.sin(direction * (180 / Math.PI));
  this.vspeed = speed * Math.cos(direction * (180 / Math.PI));

  this.updateDirection = function() {
    this.hspeed = this.speed * Math.sin(this.direction * (180 / Math.PI));
    this.vspeed = this.speed * Math.cos(this.direction * (180 / Math.PI));
  }

  this.draw = function() {
    context.fillStyle = color;
    context.globalAlpha = this.reallife / this.lifetime;
    context.beginPath();
    context.arc(this.xCoor, this.yCoor, this.radius, Math.PI * 2, false);
    context.fill();
    context.globalAlpha = 1;
  }

  this.step = function() {
    this.reallife--;
    if(this.reallife<1){
      this.reallife = this.lifetime/2 + (this.lifetime/2* Math.random());
      this.xCoor = playerx;
      this.yCoor = playery;
      this.direction = Math.random()*360;
      this.hspeed = this.speed * Math.sin(this.direction * (180 / Math.PI));
      this.vspeed = this.speed * Math.cos(this.direction * (180 / Math.PI));
    }
    this.xCoor += this.hspeed;
    this.yCoor += this.vspeed;
  }

  this.shift = function(expand) { //allow circle to grow x3 og size
    if (expand && this.lifetime<this.oglifetime*6){
      this.lifetime+=1.5;
    }else if(!expand && this.lifetime>this.oglifetime){
      this.lifetime-=.5;
      this.reallife-=.5;
    }
  }
}

function Wall(centerX, centerY, width, height){
  this.centerX = centerX;
  this.centerY = centerY;
  this.width = width;
  this.height = height;

  this.draw = function() { // for debugging wall positions
    context.globalAlpha = 1;
    context.fillRect(this.centerX-this.width/2,this.centerY-this.height/2,this.width,this.height);
  }
}

function Person(xCoor, yCoor, aura, size, color, enemy, altX, altY) {
  this.xCoor = xCoor;
  this.yCoor = yCoor;
  if(altX == 0 && altY == 0){ //if no altX and altY passed in for ghost
    this.altX = xCoor;
    this.altY = yCoor;
  }
  else{
    this.altX = altX;
	this.altY = altY;
  }
  this.auraRadius = aura;
  this.moveAmount = 1;
  this.enemy = enemy; //0: player, 1: hostile, 2: non-hostile, 3: projectile 
  this.size = size;
  this.slowed = false ;
  this.slowTime = 75;
  
    if (this.enemy ==3){
	  this.thrown = false ;
	  this.projectileAngle = 0 ;
	  this.projectileX = this.xCoor;
	  this.projectileY = this.yCoor;
	  this.projectileRadius = 10;
	  this.projectileVelocity = 5 ;
	  this.projectileLife = 50;
  } 

  this.draw = function() {
    if(this.enemy == 0) { //if player, draw based on world
	if(world == 2){context.fillStyle = "#8BBF9F"; }
	else {context.fillStyle = "#C95F45";}
    }
    else { context.fillStyle = color; }
    context.globalAlpha = 0.3;
    context.beginPath();
    context.arc(this.xCoor, this.yCoor, this.auraRadius, Math.PI * 2, false); //draw aura
    context.fill();

    context.globalAlpha = 1;
    context.beginPath();
    context.arc(this.xCoor, this.yCoor, this.size, Math.PI * 2, false); //draw character
    context.fill();

	if(this.enemy == 0) { //draw ghost
	if(world == 2){ context.fillStyle = "#C95F45"; }
	else { context.fillStyle = "#8BBF9F"; }
	context.globalAlpha = 0.5;
    context.beginPath();
    context.arc(this.altX, this.altY, this.size, Math.PI * 2, false);
    context.fill();
	}
	
	if (this.enemy==3 && this.thrown){
		context.fillStyle = 'red';
		context.globalAlpha = 1;
		context.beginPath();
		context.arc(this.projectileX, this.projectileY, this.projectileRadius, Math.PI * 2, false);
		context.fill();
	}	
  }

  this.createAura = function(amount) {
    for (var i = 0; i < amount; i++)
    particles.push(new Particle(this.xCoor, this.yCoor, 2, this.auraRadius, Math.random() * 3, 1, '#884A3B'));
  }

  this.expand = function() {
    for (var i in particles)
    particles[i].shift(true);
  }

  this.contract = function() {
    for (var i in particles)
    particles[i].shift(false);
  }

  this.update = function() {
	  
	if (this.slowed){ //projectiles slow us
		 this.slowTime--;
		 if(this.slowTime > 35){this.expand();} //and expand our aggro aura
	}
	if (this.slowTime <= 0 ){
		 this.slowed = false ;
		 this.slowTime = 75;
	 }
	 
    if(this.enemy == 0){
      this.auraRadius = aura*(particles[0].lifetime/particles[0].oglifetime);
      if(!keysPressed[shiftCode]){ //depending on our hijab state we move fast or slow
		if (!this.slowed)
			this.moveAmount = 3;
		else 
			this.moveAmount = 1;
	  }
      else if(keysPressed[shiftCode]){  
		if (!this.slowed)
			this.moveAmount = 1.75;	
		else 
			this.moveAmount = 0.75;	
				
	  }
    }
    else{this.moveAmount = 2;}

    if(this.enemy == 0){
      xPrev = this.xCoor;
      yPrev = this.yCoor;
      if(keysPressed[leftCode]){
        this.xCoor -= this.moveAmount;
        for(var i in particles)
        particles[i].xCoor -= this.moveAmount;
      }
      else if(keysPressed[rightCode]){
        this.xCoor += this.moveAmount;
        for(var i in particles)
        particles[i].xCoor += this.moveAmount;
      }
      if(keysPressed[upCode]){
        this.yCoor -= this.moveAmount;
        for(var i in particles)
        particles[i].yCoor -= this.moveAmount;
      }
      else if(keysPressed[downCode]){
        this.yCoor += this.moveAmount;
        for(var i in particles)
        particles[i].yCoor += this.moveAmount;
      }
      playerx = this.xCoor;
      playery = this.yCoor;
    }

   if(this.enemy == 0 && flip == true){ //switch to ghost
    var temp = this.xCoor;
    this.xCoor = this.altX;
    this.altX = temp;
    temp = this.yCoor;
    this.yCoor = this.altY;
    this.altY = temp;
   }
  }
}

function Pillar(x, y, rad, size){
  this.xCoor = x;
  this.yCoor = y;
  this.radius = rad;
  this.draw = function(){
    context.drawImage(pillar, x, y, size, size);
  }
}

function pressurePlate(x,y, width, height){
  this.centerX = x;
  this.centerY = y;
  this.width = width;
  this.height = height;
  this.pressed = false;

  this.checkCollision = function(personX,personY,personSize){
     dx = Math.abs(this.centerX  - personX);
     dy = Math.abs(this.centerY  - personY);
     if(dx<this.width/2 + personSize && dy<this.height/2 + personSize){
	   this.pressed = true;
     }
	 else {this.pressed = false;} 
  }

  this.draw = function(){
    if (this.pressed == false){
	context.fillStyle = 'violet';
	 }
     else{
        context.fillStyle = 'yellow';
    }
     context.fillRect(this.centerX - this.width/2 ,this.centerY-this.height/2,this.width,this.height);
  }


}

function collision(){
//level transition checks
  if(player.xCoor > 300 && player.xCoor < 500 && player.yCoor <20 && level == 1 ){
	 level = 2;
	 reset();
  }
  else if(player.xCoor > 300 && player.xCoor < 500 && player.yCoor <20 && level == 2 ){
	 level = 0;
	 reset();
  }
  else if (player.xCoor >350 && player.xCoor < 460 && player.yCoor <20 && level == 0){
     level = 3;
     reset();	 
  }
   else if (player.xCoor >325 && player.xCoor < 475 && player.yCoor <20 && level == 3){
     level = 4;
     reset();     
  } 
    else if (player.xCoor >325 && player.xCoor < 400 && player.yCoor <20 && level == 4){
     level = 5;
     reset();     
  } 
   else if (player.xCoor >220 && player.xCoor < 300 && player.yCoor <20 && level == 5){
     level = 6;
     reset();     
  } 
   else if (player.xCoor >385 && player.xCoor < 420 && player.yCoor <270 && player.yCoor >225 && level == 6){
     winning = true;     
  } 

  for(var i in enemies){//enemy collision
		var dx = enemies[i].xCoor - player.xCoor;
		var dy = enemies[i].yCoor - player.yCoor;
		var distance = Math.sqrt(dx*dx + dy*dy);
		if(enemies[i].enemy == 1 && distance < enemies[i].auraRadius + player.auraRadius && !hijab){
		  enemies[i].altY = enemies[i].yCoor ;
		  enemies[i].altX = enemies[i].xCoor ;
		  enemies[i].yCoor -= (dy * enemies[i].moveAmount)/distance;
		  enemies[i].xCoor -= (dx * enemies[i].moveAmount)/distance ;
		}
		

		if(enemies[i].enemy == 1 && distance < enemies[i].size + player.size && !hijab){ //lose state - reset level
		  reset();
		}
		else if(distance < enemies[i].size + player.size){ //if not aggro'd, push player back
		  player.xCoor = xPrev;
		  player.yCoor = yPrev;}
		
		
		if(enemies[i].enemy == 3 && distance < enemies[i].auraRadius + player.auraRadius && !enemies[i].thrown ){
				enemies[i].projectileAngle = Math.atan(dy/dx) * 180 / Math.PI;
				enemies[i].thrown = true ;
		}
		
		if (enemies[i].enemy == 3 && enemies[i].thrown ) {
				if (enemies[i].xCoor > player.xCoor){
					enemies[i].projectileX -= enemies[i].projectileVelocity*(Math.cos(enemies[i].projectileAngle*Math.PI/180));
					enemies[i].projectileY -= enemies[i].projectileVelocity*(Math.sin(enemies[i].projectileAngle*Math.PI/180));
		}
				if (enemies[i].xCoor < player.xCoor){
					enemies[i].projectileX += enemies[i].projectileVelocity*(Math.cos(enemies[i].projectileAngle*Math.PI/180));	
					enemies[i].projectileY += enemies[i].projectileVelocity*(Math.sin(enemies[i].projectileAngle*Math.PI/180));
				}
				enemies[i].projectileLife--;	
				
				var pdx = enemies[i].projectileX - player.xCoor;
				var pdy = enemies[i].projectileY - player.yCoor;
				var pDistance = Math.sqrt(pdx*pdx + pdy*pdy);
				
				
				if(pDistance < enemies[i].projectileRadius + player.size && pDistance < enemies[i].projectileRadius + player.size ){
					player.slowed = true ; 
				}
					
				
				if (enemies[i].projectileLife <= 0){
					enemies[i].thrown = false;
					enemies[i].projectileLife = 50;
					enemies[i].projectileX = enemies[i].xCoor;
					enemies[i].projectileY = enemies[i].yCoor;
				}			
		}
  }
  
  for(var i in objects[level]){//object collision
    dx = objects[level][i].xCoor + objects[level][i].radius - player.xCoor;
    dy = objects[level][i].yCoor + objects[level][i].radius  - player.yCoor;
    distance = Math.sqrt(dx*dx + dy*dy);
    if(distance < objects[level][i].radius + player.size){
      player.xCoor = xPrev;
      player.yCoor = yPrev;
    }
  }
  
  for(var i in objects[level]){
  for(var j in enemies){//enemy collision
		dx = objects[level][i].xCoor + objects[level][i].radius - enemies[j].xCoor;
		dy = objects[level][i].yCoor + objects[level][i].radius  - enemies[j].yCoor;
		var distance = Math.sqrt(dx*dx + dy*dy);
		if(distance < objects[level][i].radius + enemies[j].size){
		  enemies[j].xCoor = enemies[j].altX;
		  enemies[j].yCoor = enemies[j].altY;
		}
	
        }
  }
  
  for(var i in walls[level]){ //player collision with walls
    dx = Math.abs(walls[level][i].centerX - player.xCoor);
    dy = Math.abs(walls[level][i].centerY - player.yCoor);
    if(dx<walls[level][i].width/2+player.size && dy<walls[level][i].height/2+player.size){
     // console.log( "dx " + dx + " dy " + dy);
      player.xCoor = xPrev;
      player.yCoor = yPrev;
    }
    
	for(var j in enemies){//enemy collision with walls
	    dx = Math.abs(walls[level][i].centerX - enemies[j].xCoor);
		dy = Math.abs(walls[level][i].centerY - enemies[j].yCoor);
		var distance = Math.sqrt(dx*dx + dy*dy);
		if(dx<walls[level][i].width/2+enemies[j].size && dy<walls[level][i].height/2+enemies[j].size){
		  enemies[j].xCoor = enemies[j].altX;
		  enemies[j].yCoor = enemies[j].altY;
		}
  }
  
if(plates[level]){
 for(var i in plates[level]){
	 plates[level][i].checkCollision(player.xCoor, player.yCoor, player.size); //check with player
	 if (!plates[level][i].pressed)
		plates[level][i].checkCollision(player.altX, player.altY, player.size); //check with ghost
		 for(var j in enemies){
			if (!plates[level][i].pressed)
			plates[level][i].checkCollision(enemies[j].xCoor, enemies[j].yCoor, enemies[j].size); //check with all enemies	
		}
	 if(!plates[level][i].pressed){ //if still not pressed, draw doors
		 context.fillStyle = "purple"
		    gates[level][i].draw();
		    dx = Math.abs(gates[level][i].centerX - player.xCoor); 
			dy = Math.abs(gates[level][i].centerY - player.yCoor);
			 if(dx<gates[level][i].width/2+player.size && dy<gates[level][i].height/2+player.size){ //and prevent playyer from passing
				player.xCoor = xPrev;
				player.yCoor = yPrev;
			}
		}
	}
}
	
 }
}
initialize();

function draw(){
	globalAlpha = 1;
  canvas.width = canvas.width;

  //context.fillStyle = '#D9C183';
  if(world == 1){
	context.drawImage(tehranBackgrounds[level], 0, 0, canvas.width, canvas.height);
  }
  else if(world == 2){
	context.drawImage(parisBackgrounds[level], 0, 0, canvas.width, canvas.height);
  }
  //context.fill();

  for (var i in particles) {
    particles[i].step();
    particles[i].draw();
  }

   if(objects[level]){
	  for (iter = 0; iter < objects[level].length; iter++){
		objects[level][iter].draw();
	  }
  }
    if(plates[level]){
	  for (iter = 0; iter < plates[level].length; iter++){
		plates[level][iter].draw();
	  }
  }
  
 
 
  for ( var i in enemies){
    enemies[i].draw();
  }


    for(var i in walls[level]){//enable for debugging walls
		//walls[level][i].draw();
	  }
	   player.draw();
}

function gameLoop() {
  //console.log(player.xCoor, player.yCoor);
  if (!winning){
    if(choice == 0){
      initChoice();
    }else{
      //girl.draw();
      player.update();
      for ( var i in enemies){
        enemies[i].update();
      }
      flip = false;
      if (aura) {
        player.expand();
      }else player.contract();
      draw();
      if(player.auraRadius > 20){ hijab = false; }
      else { hijab = true; }
	    collision();
    }
  }
  else {
    canvas.width = canvas.width;
    context.drawImage(endbg,0,0);
    context.fillStyle = "white";
    context.font = "16px Arial";
    context.fillText("Thank you for playing Who Stands Before the Law.", 100, 50);
    context.fillText("This project was motivated by examining the violence and harassment muslim", 100, 100);
    context.fillText("women are subject to in France and Iran.", 100, 120);
    context.fillText("What we wanted to create was an exploration of that space, with respect to", 100, 170);
    context.fillText("our distance from it.", 100, 190);
    context.fillText("We hope that you reflect on the symmetry of these legal forces,", 100, 240);
    context.fillText("and their vast repercussions.", 100, 260);
    context.fillText("Nick Carlock", 100, 350);
    context.fillText("Koosha Sevyani", 100, 370);
    context.fillText("Jason Brisson", 100, 390);
    context.fillText("Brad Matias", 100, 410);
    

  }
}

function initialize() {
  var winning = false;
  particles = Array();

  //Room 3 (started as prototype and too much hassle to fix the number ¯\_(ツ)_/¯
  walls[0] = Array();
  walls[0].push(new Wall(45,300,90,600));//left wall
  walls[0].push(new Wall(755,300,90,600));//right wall
  walls[0].push(new Wall(200,555,285,90));//floor left
  walls[0].push(new Wall(600,555,285,90));//floor right
  walls[0].push(new Wall(400,625,800,30));//bottom cap
  walls[0].push(new Wall(0,45,680,90));//left top
  walls[0].push(new Wall(800,45,680,90));//right top

  objects[0] = Array();
	for (var iter = 0; iter < 6; iter++){
    objects[0][iter] = new Pillar(375 + (150*Math.cos(iter/3*Math.PI)), 275+ (150*Math.sin(iter/3*Math.PI)), 30, 60);
  }
  
  //Room 1
  walls[1] = Array();
  walls[1].push(new Wall(304,268,8,536));
  walls[1].push(new Wall(494,268,8,536));
  walls[1].push(new Wall(400,530,200,8));

  //room 2
  walls[2] = Array();
  walls[2].push(new Wall(292,62,15,124));
  walls[2].push(new Wall(504,62,15,124));
  walls[2].push(new Wall(138,113,270,15)); // main top left
  walls[2].push(new Wall(600,113,210,15)); // main top right
  walls[2].push(new Wall(83,317,20,425)); //main left wall
  walls[2].push(new Wall(709,317,20,425)); //main right wall
  walls[2].push(new Wall(138,520,295,20)); //main bottom left
  walls[2].push(new Wall(400,625,800,30));//bottom cap
  walls[2].push(new Wall(600,520,210,15)); //main bottom right
  walls[2].push(new Wall(292,555,15,90)); //lower left wall
  walls[2].push(new Wall(504,555,15,90)); // lower right wall
  
  //room 4
  walls[3] = Array();
  walls[3].push(new Wall(55,300,90,600));//left wall
  walls[3].push(new Wall(745,300,90,600));//right wall
  walls[3].push(new Wall(160,555,285,90));//floor left
  walls[3].push(new Wall(640,555,285,90));//floor right
  walls[3].push(new Wall(400,625,800,30));//bottom cap
  walls[3].push(new Wall(400,310,800,120));//desk
  walls[3].push(new Wall(0,60,620,130));//left top
  walls[3].push(new Wall(830,60,680,130));//right top
  
  //room 5
  walls[4] = Array();
  walls[4].push(new Wall(400,625,800,30));//bottom cap
  walls[4].push(new Wall(10,300,10,600));//left wall
  walls[4].push(new Wall(65,595,295,20)); //bottom left
  walls[4].push(new Wall(570,595,500,15)); //main bottom right
  walls[4].push(new Wall(165,5,295,20)); //upper left
  walls[4].push(new Wall(790,300,10,600));//right wall
  walls[4].push(new Wall(620,8,400,20));//right top
  walls[4].push(new Wall(-25,225,230,35)); //upper left protrusion
  walls[4].push(new Wall(190,225,100,35)); //reverse L bottom
  walls[4].push(new Wall(225,115,30,200)); //reverse L side
  walls[4].push(new Wall(560,95,30,200)); //right top protrusion
  walls[4].push(new Wall(560,320,30,150)); //right bottom protrusion
  walls[4].push(new Wall(110,380,520,35)); //middle left
  walls[4].push(new Wall(680,380,520,35)); //middle right
  
  plates[4] = Array();
  plates[4].push(new pressurePlate(398, 420, 60, 60))
  plates[4].push(new pressurePlate(160, 130, 30, 30))
  plates[4].push(new pressurePlate(680, 140, 40, 40))
  plates[4].push(new pressurePlate(290, 250, 40, 40))
  plates[4].push(new pressurePlate(460, 250, 40, 40))

  
  gates[4] = Array();
  gates[4].push(new Wall(398, 380, 50, 30));
  gates[4].push(new Wall(561, 218, 33, 50));
  gates[4].push(new Wall(110, 225, 54, 32));
  gates[4].push(new Wall(370, 0, 120, 50))
  gates[4].push(new Wall(370, 0, 120, 50))


  
  
  //room 6
  walls[5] = Array();
  walls[5].push(new Wall(10,300,10,600));//left wall
  walls[5].push(new Wall(790,300,10,600));//right wall
  walls[5].push(new Wall(400,590,800,10));//bottom
  walls[5].push(new Wall(2,10,400,10));//top
  walls[5].push(new Wall(722,10,800,10));//top
  walls[5].push(new Wall(140,405,275,85));//bottom left protrusion
  walls[5].push(new Wall(35,158,45,35));//top left protrusion
  walls[5].push(new Wall(302,158,389,35));//top of L
  walls[5].push(new Wall(480,280,35,275));//right of L
  walls[5].push(new Wall(480,530,35,125));//bottom right protrusion
  walls[5].push(new Wall(175,77,55,125));//top left column
  walls[5].push(new Wall(338,20,33,50));//short column top
  walls[5].push(new Wall(338,120,33,50));//short column bot
  
  plates[5] = Array();
  plates[5].push(new pressurePlate(195, 335, 35, 35))
  plates[5].push(new pressurePlate(450, 120, 40, 40))
  plates[5].push(new pressurePlate(80, 80, 40, 40))

    
  gates[5] = Array();
  gates[5].push(new Wall(485, 440, 40, 50));
  gates[5].push(new Wall(83, 160, 53, 25));
  gates[5].push(new Wall(335, 70, 30, 50))
  
  //room 7
    walls[6] = Array();
    walls[6].push(new Wall(400,625,800,30));//bottom cap
  walls[6].push(new Wall(10,300,10,600));//left wall
  walls[6].push(new Wall(65,595,550,20)); //bottom left
  walls[6].push(new Wall(650,595,400,15)); //main bottom right
  walls[6].push(new Wall(165,5,600,20)); //upper left
  walls[6].push(new Wall(790,300,10,600));//right wall
  walls[6].push(new Wall(620,8,400,20));//right top
  
  plates[6] = Array();
  plates[6].push(new pressurePlate(400, 440, 35, 35))
  plates[6].push(new pressurePlate(400, 120, 30, 30))

    
  gates[6] = Array();
  gates[6].push(new Wall(400, 95, 115, 10));
  gates[6].push(new Wall(400, 370, 65, 10));
  
  
  //pillars for level 6
  objects[6] = Array ();
  objects[6].push(new Pillar(311, 506, 20, 40));
  objects[6].push(new Pillar(449, 506, 20, 40));
  objects[6].push(new Pillar(295, 456, 20, 40));
  objects[6].push(new Pillar(465, 456, 20, 40));
  objects[6].push(new Pillar(311, 406, 20, 40));
  objects[6].push(new Pillar(449, 406, 20, 40));
  objects[6].push(new Pillar(329, 356, 20, 40));
  objects[6].push(new Pillar(431, 356, 20, 40));
  objects[6].push(new Pillar(305, 140, 20, 40));
  objects[6].push(new Pillar(455, 140, 20, 40));
  objects[6].push(new Pillar(305, 90, 20, 40));
  objects[6].push(new Pillar(455, 90, 20, 40));
  objects[6].push(new Pillar(355, 140, 20, 40));
  objects[6].push(new Pillar(405, 140, 20, 40));
  objects[6].push(new Pillar(311, 550, 20, 40));
  objects[6].push(new Pillar(449, 550, 20, 40));
  
 
  player = new Person(400, 500, 20, 15, "green", 0,0,0); //xCoor, yCoor, aura, size, color, enemy
  player.createAura(80);
  
  reset();

}

setInterval(gameLoop, 16);

function initChoice() { //opening menu
  canvas.width = canvas.width;
  var bg = new Image();
  bg.src = "http://i.imgur.com/gmywCv0.png";
//  var withh = new Image();
  //withh.src =  "http://people.ucsc.edu/~ncarlock/pics/Hijab1.jpg";
  //var without = new Image();
  //without.src = "http://people.ucsc.edu/~ncarlock/pics/WithoutHijab1.jpg";
  context.drawImage(bg,0,0);
  //context.drawImage(withh, 200, 100, 429, 136);
  //context.drawImage(without, 200, 300, 429, 136);
  context.beginPath();
  if(keysPressed[upCode] || select == 1){
    select = 1;
    context.beginPath();
    context.strokeStyle = "white";
    context.lineWidth = 4;
    //context.rect(200, 100, 430, 137);
    //context.stroke();
    context.arc(400, 331, 57, Math.PI * 2, false);
    context.stroke();
    context.lineWidth = 1;
  }
  if(keysPressed[downCode] || select == 2){
    select = 2;
    context.beginPath();
    context.strokeStyle = "white";
    context.lineWidth = 4;
    context.arc(400, 489, 57, Math.PI * 2, false);
    context.stroke();
    context.lineWidth = 1;
  }
  if(select == 1 && keysPressed[enterCode]){choice = 1;}
  else if(select == 2 && keysPressed[enterCode]){choice = 2;}
world = choice;
}

function reset(){
	world = choice;
	aura = false;
	for(var i in particles){
	particles[i].lifetime = 20;
	}
    if(level == 0){
		enemies = Array();
		player = new Person(400, 560, 20, 15, "green", 0,170,440); //xCoor, yCoor, aura, size, color, enemy
	 for (var iter = 0; iter < 6; iter++){
    enemies.push(new Person(403 + (150*Math.cos(iter/3*Math.PI + Math.PI/6)), 303+ (150*Math.sin(iter/3*Math.PI+ Math.PI/6)), 30, 20, "blue", 1, 0, 0));
	 }
  enemies.push(new Person(280, 125, 30, 19, "blue", 1, 0, 0));
  enemies.push(new Person(520, 125, 30, 19, "blue", 1, 0, 0));
	}
	if(level == 1){
	    enemies = Array();
		player = new Person(400, 500, 20, 15, "green", 0,0,0); //xCoor, yCoor, aura, size, color, enemy
	}
	if(level == 2){
	    enemies = Array();
		player = new Person(400, 580, 20, 15, "green", 0,0,0); //xCoor, yCoor, aura, size, color, enemy
	    enemies[0] = new Person(345, 150, 30, 20, "blue", 1, 0, 0);
	    enemies[1] = new Person(400, 150, 30, 20, "blue", 1, 0, 0);
		enemies[2] = new Person(455, 150, 30, 20, "blue", 1, 0, 0);
	}
	if(level == 3){
		enemies = Array();
		player = new Person(400, 580, 20, 15, "green", 0,400,180); //xCoor, yCoor, aura, size, color, enemy
	}
	if(level == 4){
		enemies = Array();
		player = new Person(265, 570, 20, 15, "green", 0,65,130); //xCoor, yCoor, aura, size, color, enemy
		enemies[0] = new Person(398, 420, 30, 20, "blue", 1, 0, 0);
		enemies[1] = new Person(638, 300, 30, 20, "blue", 1, 0, 0);
		enemies[2] = new Person(738, 300, 30, 20, "blue", 1, 0, 0);
		enemies[3] = new Person(375, 220, 30, 20, "blue", 1, 0, 0);
	}
	if(level == 5){
	enemies = Array();
		player = new Person(60, 550, 20, 15, "green", 0,0,0); //xCoor, yCoor, aura, size, color, enemy
		enemies[0] = new Person(245, 515, 50, 20, "red", 3, 0, 0);
		enemies[1] = new Person(245, 200, 50, 20, "red", 3, 0, 0);
		enemies[2] = new Person(415, 235, 30, 20, "blue", 1, 0, 0);
		enemies[3] = new Person(735, 375, 30, 20, "blue", 1, 0, 0);
		enemies[4] = new Person(545, 375, 30, 20, "blue", 1, 0, 0);
		enemies[5] = new Person(640, 375, 50, 20, "red", 3, 0, 0);
		enemies[6] = new Person(555, 250, 50, 20, "red", 3, 0, 0);
		enemies[7] = new Person(640, 250, 30, 20, "blue", 1, 0, 0);
		enemies[8] = new Person(115, 335, 30, 20, "blue", 1, 0, 0);
		enemies[9] = new Person(150, 200, 50, 20, "RED", 3, 0, 0);
		enemies[10] = new Person(730, 250, 50, 20, "red", 3, 0, 0);
		enemies[11] = new Person(120, 40, 30, 20, "blue", 1, 0, 0);
	}
	if(level == 6){
	enemies = Array();
		player = new Person(399, 575, 20, 15, "green", 0,100,520); //xCoor, yCoor, aura, size, color, enemy
		aura = false;
	    //enemies[0] = new Person(308, 560, 30, 20, "blue", 1, 0, 0);
		enemies[1] = new Person(399, 520, 30, 20, "blue", 1, 0, 0);
		//enemies[2] = new Person(490, 560, 30, 20, "blue", 1, 0, 0);
		enemies[3] = new Person(200, 560, 50, 20, "red", 3, 0, 0);
		enemies[4] = new Person(600, 560, 50, 20, "red", 3, 0, 0);
		enemies[5] = new Person(300, 200, 50, 20, "red", 3, 0, 0);
		enemies[6] = new Person(280, 260, 50, 20, "red", 3, 0, 0);
		enemies[7] = new Person(300, 325, 50, 20, "red", 3, 0, 0);
		enemies[8] = new Person(500, 200, 50, 20, "red", 3, 0, 0);
		enemies[9] = new Person(520, 260, 50, 20, "red", 3, 0, 0);
		enemies[10] = new Person(500, 325, 50, 20, "red", 3, 0, 0);
		enemies[11] = new Person(250, 180, 30, 20, "blue", 1, 0, 0);
		enemies[12] = new Person(220, 260, 30, 20, "blue", 1, 0, 0);
		enemies[13] = new Person(250, 340, 30, 20, "blue", 1, 0, 0);
		enemies[14] = new Person(550, 180, 30, 20, "blue", 1, 0, 0);
		enemies[15] = new Person(580, 260, 30, 20, "blue", 1, 0, 0);
		enemies[16] = new Person(550, 340, 30, 20, "blue", 1, 0, 0);
 }
}
